<?php
require_once 'persistencia/Conexion.php';//importar conexion
require_once 'persistencia/InventarioDAO.php';//importar InventarioDAO

class Inventario{//datos de inventario
    private $id_prod;
    private  $nombre;
    private  $cant;
    private  $InventarioDAO;
    
    
    //get y set - ID del producto
    public function getId_prod()
    {
        return $this->id_prod;
    }

    public function setId_prod($id_prod)
    {
        $this->id_prod = $id_prod;
    }
    
    //get - Nombre del producto
    public function getNombre()
    {
        return $this->nombre;
    }
    
    //get y set - Cantidad del producto en inventario
    public function getCant()
    {
        return $this->cant;
    }
  
    public function setCant($cant)
    {
        $this->$cant = $cant;
    }
   
    //Constructor de la clase inventario
    public function __construct($id_prod="", $nombre="", $cant=""){
        $this->id_prod = $id_prod;
        $this->nombre=$nombre;
        $this->cant = $cant;
        $this->conexion= new Conexion();
        $this->InventarioDAO = new InventarioDAO($this->id_prod, $this->nombre, $this->cant);
    }

    //Funcion crear objeto inventario
    public function crear(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->InventarioDAO->crear());//crear objeto inventario
        $this->conexion->cerrar();
    }

    //Funcion consultar inventario
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->InventarioDAO->consultar());
        $registro= $this->conexion->extraer();
        $id_tien = new Tienda($registro[0]);
        $id_tien -> consultar();
        $this -> id_tien = $id_tien;
        $this->nombre=$registro[1];
        $this->imagen=$registro[2];
        $this->valor=$registro[3];
        $this->descripcion=$registro[4];
        $this->conexion->cerrar();
        
    }
    
    //Funcion consultar todos los objetos de inventario
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->productoDAO->consultarTodos());
        
        $productos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_tien = new Tienda($registro[1]);
            $id_tien -> consultar();
            $this -> id_tien = $id_tien;
            $producto = new Producto($registro[0], $id_tien, $registro[2], $registro[3], $registro[4], $registro[5]);
            array_push($productos, $producto);
        }
        $this -> conexion -> cerrar();
        return  $productos;
    }
    
    //
    public function consultarProTienda(){
        $this->conexion->abrir();
        $this->productoDAO -> setId_tien($this->id_tien);
        $this->conexion->ejecutar($this->productoDAO->consultarProTienda());
        
        $productos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_tien = new Tienda($registro[1]);
            $id_tien -> consultar();
            $this -> id_tien = $id_tien;
            $producto = new Producto($registro[0], $id_tien, $registro[2], $registro[3], $registro[4], $registro[5]);
            array_push($productos, $producto);
        }
        $this -> conexion -> cerrar();
        return  $productos;
        
        
    }
  
    //
    public function consultarMonto(){
        $this->conexion->abrir();
        $this->productoDAO->setId($this->id);
        $this->conexion->ejecutar($this->productoDAO->consultarMonto());
        
        $productos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_tien = new Tienda($registro[1]);
            $id_tien -> consultar();
            $this -> id_tien = $id_tien;
            $producto = new Producto($registro[0], $id_tien, $registro[2], $registro[3], $registro[4], $registro[5]);
            array_push($productos, $producto);
        }
        $this -> conexion -> cerrar();
        return  $productos;
        
    }

    //
    public function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> buscar($filtro));
        
        $productos = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_tien = new Tienda($registro[1]);
            $id_tien -> consultar();
            $this -> id_tien = $id_tien;
            $producto = new Producto($registro[0], $id_tien, $registro[2], $registro[3], $registro[4], $registro[5]);
            array_push($productos, $producto);
        }
        $this -> conexion -> cerrar();
        return  $productos;
    }

   
}
?>