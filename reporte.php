<?php 
require_once 'logica/Administrador_ma.php';//Importar Administrador y sus funciones
require_once 'logica/Cliente.php';//Importar Cliente y sus funciones
require_once 'logica/Tendero.php';
require_once 'logica/Tienda.php';
require_once 'logica/Producto.php';//Importar Tienda y sus funciones
require_once 'logica/Carrito.php';
require_once 'logica/Venta.php';
require_once 'fpdf/fpdf.php';




$venta= new Venta();

$ventas= $venta->consultarTodos();

$pdf = new FPDF('P','mm', 'Letter');
$pdf -> SetMargins(10, 10, 10);
$pdf -> AddPage();

$pdf -> SetFont('Times', 'B', 18);

$pdf -> Cell(196, 10, "SHOPINT", 0, 1, 'C');

$pdf -> Cell(196, 20, "Reporte ventas por tienda", 0, 1, 'C');

$pdf -> SetFont('Times', 'B', 10);


$pdf -> Cell(10, 8, "#", 1, 0, 'C');
$pdf -> Cell(47, 8, "Nombre tienda", 1, 0, 'C');
$pdf -> Cell(30, 8, "Fecha venta", 1, 0, 'C');
$pdf -> Cell(28, 8, "Nombre Cliente", 1, 0, 'C');
$pdf -> Cell(30, 8, "Nombre Producto", 1, 0, 'C');
$pdf -> Cell(20, 8, "Cantidad", 1, 0, 'C');
$pdf -> Cell(30, 8, "Total a Pagar", 1, 1, 'C');

$pdf -> SetFont('Times', '', 10);
$i = 1;
foreach ($ventas as $ventaActual){
    $pdf -> Cell(10, 8, $i++, 1, 0, 'C');
    $pdf -> Cell(47, 8, $ventaActual ->getId_tend()->getNombre() , 1, 0, 'C');
    $pdf -> Cell(30, 8, $ventaActual -> getFecha() , 1, 0, 'C');
    $pdf -> Cell(28, 8, $ventaActual ->getId_cli()-> getNombre() , 1, 0, 'C');
    $pdf -> Cell(30, 8, $ventaActual ->getId_pro()->getNombre() , 1, 0, 'C');
    $pdf -> Cell(20, 8,  $ventaActual -> getCantidad() , 1, 0, 'C');
    $pdf -> Cell(30, 8, "$". $ventaActual -> getTotal() , 1, 1, 'C');
  }
$pdf -> Output('I');
?>