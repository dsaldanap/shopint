<?php
require_once 'persistencia/Conexion.php';//importar conexion
require_once 'persistencia/DueñoDAO.php';//importar AdmistradorDao
class Dueño{
    private $id;
    private $nombre;
    private  $usuario;
    private  $clave;
    private  $conexion;
    private  $dueñoDAO;
    
    //get id dueño
    public function getId()
    {
        return $this->id;
    }

   //get nombre dueño
    public function getNombre()
    {
        return $this->nombre;
    }

    //get usuario dueño
    public function getUsuario()
    {
        return $this->usuario;
    }

    //constructor clase dueño
    public function __construct($id="", $nombre="", $usuario="", $clave=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->usuario=$usuario;
        $this->clave=$clave;
        $this->conexion= new Conexion();
        $this->dueñoDAO=new DueñoDAO($this->id, $this->nombre, $this->usuario, $this->clave);
        
    }

    //funcion autenticar Dueño
    public  function autenticar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->dueñoDAO->autenticar());
        $this->conexion->cerrar();
        if ($this->conexion->numFilas()==1){
            $this->id= $this->conexion->extraer()[0];
            return true;
        }
        else {
            return false;
        }
        
        
    }
    
    //Funcion consultar dueño
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->dueñoDAO->consultar());
        $this->conexion->cerrar();
        $datos= $this->conexion->extraer();
        $this->nombre=$datos[0];
        $this->usuario=$datos[1];
    }
   
}
?>