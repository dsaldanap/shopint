<?php include 'presentacion/vistaCli.php';
$carrito = new Carrito();
$id_cli=$cliente->getId();
$fechaActual = date('Y-m-d');
$carrito =new Carrito("","",$id_cli,"", "","");
$carritos= $carrito->consultarCar();
$compra=new Compra();
$i=1;
$total = 0;
if(isset($_POST["eli"])){
    $name=$_POST["eli"];
    $carrito2= new Carrito($name,"","","", "","");
   $carritos2= $carrito2->borrar();
}
?>
<div class="container">
    <div class="row mt-3">
        <?php if(isset($_POST["crear"])){?>
        <div class="alert alert-success alert-dismissible fade show col-3-md-1 text-center" role="alert">
            Productos comprados correctamente!
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>

        <?php }?>
        <div class="col-12">
            <div class="card">
                <h3 class="card-header text-center">Consultar Carrito</h3>

                <form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/verCarrito.php")?>">
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead class="text-center">
                                <tr>
                                    <th>Ítem</th>
                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                    <th>Valor</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <?php 
							
							foreach ($carritos as $carritoActual){?>
                                <tr>
                                    <td><?php echo $i++?></td>
                                    <td><?php echo $carritoActual -> getNombre() ?></td>
                                    <td><?php echo  $carritoActual -> getCantidad()?></td>
                                    <td><?php echo $carritoActual -> getMonto()?></td>
                                    <?php  $total = ($total + ($carritoActual -> getMonto()) );?>

                                    <td>

                                        <button value="<?php echo $carritoActual->getId();?>" name="eli"
                                            class="btn btn-outline-danger text-center">Eliminar</button>
                                    </td>


                                </tr>
                                <?php	}?>

                            </tbody>

                        </table>

                    </div>

                    <div class="card-footer d-flex">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success text-center" name="crear"
                                href="google.com">Generar
                                Venta</button>


                        </div>
                        <div class="col-sm-1 ml-auto">
                            <label>Total:</label>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="txtTotal" value="$ <?php echo $total ?>" class="form-control "
                                readonly>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
  
    if(isset($_POST["crear"])){  
        $i--;
        $montoto=$_POST["txtTotal"];
       
        $compra= new Compra('',$id_cli,$fechaActual,$i,$total);
        $compra->crear();
        $carrito -> setId_cli($id_cli);
        $carritos= $carrito->borrar2();
       $i=0;
       }
   
    ?>