<?php include 'presentacion/vistaDue.php';
$compra= new Compra();

$compras= $compra->consultarTodos();
?>
<div class="container">
    <div class="row mt-3">
        
        <div class="col-12">
        <br>
            <div class="card">
               
                <h3 class="card-header text-center">Consultar Ventas</h3>
                <div class="card-body">
    <table class="table table-hover text-center">
         <thead>
             <tr>
                 <th># Compra </th>
                 <th>Nombre Cliente</th>
                 <th>Fecha compra</th>
                 <th>Numero de Productos</th>
                 <th>Total</th>
             </tr>
         </thead>
         <tbody class="text-center">
                               <?php 
							$i = 1;
							foreach ($compras as $compraActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $compraActual ->getId_clie()->getNombre() . "</td>";
                                echo "<td>" . $compraActual -> getFecha() . "</td>";
                                echo "<td>" . $compraActual ->getCantidad(). "</td>";
							   
                                echo "<td>" . "$".$compraActual -> getMontoto() . "</td>";
							    echo "</tr>";						    
							}
						?>
                            </tbody>
     </table>

                </div>
            </div>
    </div>
</div>