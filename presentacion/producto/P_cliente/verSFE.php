<?php include 'presentacion/vistaCli.php';
$producto = new Producto();
$id_tien = '2';
$producto -> setId_tien($id_tien);
$productos= $producto->consultarProTienda();

if(isset($_POST["crear"])){
    $id_cli=$cliente->getId();
    $id =$_POST["id"];
    $valor=$_POST["val"];
    $cant=$_POST["cant"];
    $pro = new Producto();
    $total= $valor*$cant;
   
    $nombre=$_POST["nomb"];
    
    $car= new Carrito("", $id, $id_cli,"","", "");
    $cars = $car->consultarCanti2();
    if($cars==true){
        $cars = $car->consultarCanti();
        $cantbd=$car->getCantidad();
        $idcar=$car->getId();
       $totcant=$cantbd+$cant;
       $total2=$totcant*$valor;
        $carrito= new Carrito($idcar,$_POST["id"],$id_cli,$nombre,$totcant,$total2);
   
        $carrito->actualizar();
    }
    else{
        $carrito= new Carrito('',$_POST["id"],$id_cli,$nombre,$cant,$total);
   
    $carrito->crear();
    }
  
  
}
?><div class="container">
<br>
<div class="row">
    <?php if(isset($_POST["crear"])){?>
    <div class="alert alert-success alert-dismissible fade show col-3-md-1 text-center" role="alert">
        Producto añadido al carrito!!
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>

    <?php }?>
    <?php 

        foreach ($productos as $productoActu) { ?>

    <div class="col-md-3" value="<?php echo $productoActu->getId();?>">
        <form method="post">
            <div class="card">
               

                <input type="hidden" name="id" class="form-control" value="<?php echo $productoActu->getId();?>">
                <img class="card-img-top" src="presentacion/img/<?php echo $productoActu->getImagen();?>"
                    width="300" height="300"">
                <div class=" card-body">
                <div class="mb-3 text-center">
                    <input type="hidden" name="nomb" class="form-control"
                        value="<?php echo $productoActu->getNombre();?>">
                    <h3 class="card-title"><?php echo $productoActu->getNombre();?></h3>
                </div>
                <div class="mb-3 text-center">
                    <input type="hidden" name="val" class="form-control"
                        value="<?php echo $productoActu->getValor();?>">
                    <h6 class="card-title" name="val">Valor: $<?php echo $productoActu->getValor();?></h6>
                </div>
                
                <h6 class="card-title">Descripción: <?php echo $productoActu->getDescripcion();?></h6>
                <div class="form-group d-flex">
                    <div class="col-sm-5 d-flex text-center">
                        <label class="form-label">Cantidad</label>

                    </div>
                    <div class="col-sm-3">

                        <input type="number" name="cant" class="form-control" value='1' min='1'>
                        </br>
                    </div>
                </div>

                <div class="text-center">

                    <button name="crear" class="btn btn-outline-success text-center">Agregar al carrito </button>

                </div>

            </div>
    </div>
    </form>
</div>

<?php } ?>

</div>
</div>