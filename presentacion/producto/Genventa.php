<?php
include 'presentacion/vistaTienda.php';
$producto = new Producto();
$id_tien =$tendero->getId();
$producto -> setId_tien($id_tien);
$productos= $producto->consultarProTienda();
$cliente = new Cliente();
$clientes = $cliente->consultarTodos();
$venta = new Venta();
$id_tende=$tendero->getId();


$fechaActual = date('Y-m-d');
if(isset($_POST["crear"])){  
    $pro = new Producto(); 
    $id_pro=$_POST["prod"];
   
  $pro->setID($id_pro);
  $pros = $pro->consultarMonto();
  
  $cant=$_POST["cant"];
 $valor =$pro->getValor();
 $total= $valor*$cant;
  $venta= new Venta('',$_POST["cliente"],$id_tende,$id_pro,$total,$fechaActual,$cant);
    
  $venta->crear();
    
}
?>
<div class="container">
    <div class="row mt-3">
        <div class="col-4">
        </div>
        <div class="col-4">
            <div class="card">
                <h3 class="card-header text-center">Generar Venta</h3>
                <div class="card-body">
<?php if(isset($_POST["crear"])){?>

    <div class="alert alert-success alert-dismissible fade show" role="alert">
    Venta generada exitosamente
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>

   <?php }?>
                    <form method="post" enctype="multipart/form-data">
                       
                       
						<div class="mb-4 text-center" >
							<label class="form-label">Cliente</label> <select
								class="form-select text-center" name="cliente">
                            <?php
                            foreach ($clientes as $cliActual)
                                echo "<option value='" . $cliActual->getId() . "'>" . $cliActual->getNombre() . "</option>";
                            ?>
                            </select>
						</div>
                        
                        <div class="mb-4 text-center">
							<label class="form-label">Producto</label> <select
								class="form-select text-center" name="prod">
                            <?php
                            foreach ($productos as $proActual)
                                echo "<option value='" . $proActual->getId() . "'>" . $proActual->getNombre() ." = $". $proActual->getValor(). "</option>";
                                

                            ?>
                            </select>
				
                        <div class="mb-2 text-center">
                            <label class="form-label">Cantidad</label>
                            <input type="number" class="form-control text-center" name="cant" value="1" min="1" >
                        </div>
      
                        <div class="mb-3 text-center">
                            <button type="submit" class="btn btn-primary text-center" name="crear" >Crear</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
