<?php
require_once 'persistencia/Conexion.php';//importar conexion
require_once 'persistencia/CompraDAO.php';//importar productoDao

class Compra  {//datos de venta
    private  $id;
    private  $id_clie;
    private  $fecha;
    private  $num_pro;
    private  $montoto;
    private  $conexion;
    private  $ventaDAO;
    
    
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    public function getId_clie()
    {
        return $this->id_clie;
    }
 
    
    /**
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }
   
    
    /**
     * @return string
     */
    
    public function getCantidad()
    {
        return $this->num_pro;
    }
    public function getMontoto()
    {
        return $this->montoto;
    }
    
    
    
    
    public function __construct($id="",  $id_clie="", $fecha="",$num_pro="",  $montoto=""){
        $this->id=$id;
        $this->id_clie=$id_clie;
        $this->fecha=$fecha;
       
        $this->num_pro=$num_pro;
        $this->montoto=$montoto;
        $this->conexion= new Conexion();
        $this->compraDAO=new CompraDAO($this->id, $this->id_clie, $this->fecha, $this->num_pro,$this->montoto);
        
    }
    public function crear(){
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->compraDAO->crear());//crear producto
        
        $this->conexion->cerrar();
    }
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->consultar());
        $registro= $this->conexion->extraer();
        $id_clie = new Cliente($registro[0]);
        $id_clie -> consultar();
        $this->id_clie=$id_clie;
        $this->fecha=$registro[1];
        $this->num_pro=$registro[2];
        $this->montoto=$registro[3];
        $this->conexion->cerrar();
        
    }
    public function consultarComprasPorCliente(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->consultarComprasPorCliente());
        $compras = array();
        while(($registro = $this -> conexion -> extraer()) != null){
    
            array_push($compras, $registro);
        }
        $this -> conexion -> cerrar();
        return  $compras;
        
        
    }
    public function borrar()
    {
        $this->conexion->abrir();
       
        $this->conexion->ejecutar($this->compraDAO->borrar()); // crear producto

        $this->conexion->cerrar();
    }
    
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->consultarTodos());
        $compras = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_clie = new Cliente($registro[1]);
            $id_clie -> consultar();
            $this -> id_clie = $id_clie;
           
            $compra = new Compra($registro[0], $id_clie,$registro[2],$registro[3],$registro[4]);
            array_push($compras, $compra);
        }
        $this -> conexion -> cerrar();
        return  $compras;
        
        
    }
    public function consultarCompras(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->consultarCompras());
        $compras = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_clie = new Cliente($registro[1]);
            $id_clie -> consultar();
            $this -> id_clie = $id_clie;
           
            $compra = new Compra($registro[0], $id_clie,$registro[2],$registro[3],$registro[4]);
            array_push($compras, $compra);
        }
        $this -> conexion -> cerrar();
        return  $compras;
        
        
    }
    public function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> compraDAO -> buscar($filtro));
        $compras = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_clie = new Cliente($registro[1]);
            $id_clie -> consultar();
            $this -> id_clie = $id_clie;
           
            $compra = new Compra($registro[0], $id_clie,$registro[2],$registro[3],$registro[4]);
            array_push($compras, $compra);
        }
        $this -> conexion -> cerrar();
        return  $compras;



    }
    
    
    
    
    
    
}
?>