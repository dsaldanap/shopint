<?php 
$filtro = $_GET["filtro"];
if($filtro=='tod'){
    $venta= new Venta();
    $ventas = $venta -> consultarTodos();
}else{
    $venta = new Venta();
    $ventas = $venta -> buscar($filtro);
}

?>
<table class="table table-striped table-hover">
<thead class="text-center"> 
             <tr>
                 <th># Venta </th>
                 <th>Nombre tienda</th>
                 <th>Fecha venta</th>
                 <th>Nombre Cliente</th>
                 <th>Producto</th>
                 
                 <th>Cantidad</th>
                 <th>Total</th>
             </tr>
         </thead>
         <tbody class="text-center">
                                <?php 
							$i = 1;
							foreach ($ventas as $ventaActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $ventaActual ->getId_tend()->getNombre() . "</td>";
                                echo "<td>" . $ventaActual -> getFecha() . "</td>";
							    echo "<td>" . $ventaActual ->getId_cli()-> getNombre() . "</td>";
                                echo "<td>" . $ventaActual ->getId_pro()->getNombre(). "</td>";
							    echo "<td>" . $ventaActual -> getCantidad() . "</td>";
                                echo "<td>" . "$".$ventaActual -> getTotal() . "</td>";
							    echo "</tr>";							    
							}
						?>
                            </tbody>
     </table>