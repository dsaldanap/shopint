<?php
include 'presentacion/vistaDue.php';

$venta= new Venta();
$tendero = new Tendero();

$ventas= $venta->consultarVentasPorTienda();
$ventasTend= $venta->consultarVentasPorTendero();
$compra= new Compra();

$compras= $compra->consultarComprasPorCliente();

?>
<div class="container">
	<div class="row mt-3">
        <h3 class="text-center">Estadisticas</h3>
		<div class="col">
			<div class="card">
				<h5 class="card-header">Ventas por tienda</h5>
				<div class="card-body">
					<div id="piechart" style="height: 500px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Diagrama circular -->
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Producto', 'Cantidad'],
      <?php 
          foreach ($ventas as $ventaActual ){
              echo "['" . $ventaActual[0] . "', " . $ventaActual[1] . "],";
          }      
      ?>
    ]);
    
    var options = {
      title: 'Productos vendidos por tienda'
    };    
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));    
    chart.draw(data, options);
}
</script>

<!-- Diagrama de barras horizontales -->
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Compra por cliente</h5>
				<div class="card-body">
					<div id="barchart_values" style="height: 700px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Producto', 'Cantidad'],
      <?php 
          foreach ($compras as $compraActual ){
              echo "['" . $compraActual[0] . "', " . $compraActual[1] ."],";
          }      
      ?>
    ]);
    
    var options = {
      title: 'Compras por cliente',
      
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    };    
    var chart = new google.visualization.BarChart(document.getElementById('barchart_values'));    
    chart.draw(data, options);
}
</script>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Ventas por tendero</h5>
				<div class="card-body">
					<div id="piechart2" style="height: 500px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
google.charts.load('current', {'packages':['bar']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

  var data = google.visualization.arrayToDataTable([
      ['Tendero', 'Cantidad'],
      <?php 
          foreach ($ventasTend as $ventaActual ){
              echo "['" . $ventaActual[0] . "', " . $ventaActual[1] . "],";
          }      
      ?>
    ]);
    
    var options = {
          chart: {
          }
        };

        var chart = new google.charts.Bar(document.getElementById('piechart2'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>