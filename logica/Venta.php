<?php
require_once 'persistencia/Conexion.php';//importar conexion
require_once 'persistencia/VentaDAO.php';//importar productoDao

class Venta  {//datos de venta
    private  $id;
    private  $id_cli;
    private  $id_tend;
    private  $id_prod;
    private  $total;
    private  $fecha;
    private  $cantidad;
    private  $conexion;
    private  $ventaDAO;
    
    
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    public function getId_cli()
    {
        return $this->id_cli;
    }
    /**
     * @return string
     */
    public function getId_tend()
    {
        return $this->id_tend;
    }
    
    /**
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }
    
    public function getId_pro()
    {
        return $this->id_prod;
    }
    
    /**
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }
    public function getCantidad()
    {
        return $this->cantidad;
    }
    
    
    
    
    
    public function __construct($id="",  $id_cli="", $id_tend="",$id_prod="",  $total="", $fecha="",$cantidad=""){
        $this->id=$id;
        $this->id_cli=$id_cli;
        $this->id_tend=$id_tend;
        $this->total=$total;
        $this->fecha=$fecha;
        $this->id_prod=$id_prod;
        $this->cantidad=$cantidad;
        $this->conexion= new Conexion();
        $this->ventaDAO=new VentaDAO($this->id, $this->id_cli, $this->id_tend, $this->id_prod,$this->total, $this->fecha,$this->cantidad);
        
    }
    public function crear(){
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->ventaDAO->crear());//crear producto
        
        $this->conexion->cerrar();
    }
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->compraDAO->consultar());
        $registro= $this->conexion->extraer();
        $id_cli = new Cliente($registro[0]);
        $id_cli -> consultar();
        $this->id_cli=$id_cli;
        $id_tend = new Tendero($registro[1]);
        $id_tend -> consultar();
        $this -> id_tend = $id_tend;
        $id_prod = new Producto($registro[2]);
        $id_prod->consultar();
        $this->id_pro = $id_pro;
        $this->total=$registro[3];
        $this->fecha=$registro[4];
        $this->cantidad=$registro[5];
        $this->conexion->cerrar();
        
    }
    
    
    public function consultarTodos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->ventaDAO->consultarTodos());
        
        $ventas = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_cli = new Cliente($registro[1]);
            $id_cli -> consultar();
            $this -> id_cli = $id_cli;
            $id_tend = new Tienda($registro[2]);
            $id_tend -> consultar();
            $this -> id_tend = $id_tend;
            $id_prod = new Producto($registro[3]);
            $id_prod->consultar();
            $this->id_prod = $id_prod;
            $venta = new Venta($registro[0], $id_cli ,$id_tend, $id_prod,$registro[4],$registro[5],$registro[6]);
            array_push($ventas, $venta);
        }
        $this -> conexion -> cerrar();
        return  $ventas;
        
        
    }
    public function consultarVentasPorTienda(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->ventaDAO->consultarVentasPorTienda());
        $ventas = array();
        while(($registro = $this -> conexion -> extraer()) != null){
    
            array_push($ventas, $registro);
        }
        $this -> conexion -> cerrar();
        return  $ventas;
        
        
    }
    
    public function consultarVentasPorTendero(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->ventaDAO->consultarVentasPorTendero());
        $ventas = array();
        while(($registro = $this -> conexion -> extraer()) != null){
    
            array_push($ventas, $registro);
        }
        $this -> conexion -> cerrar();
        return  $ventas;
        
    }
    
    public function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ventaDAO -> buscar($filtro));
        $ventas = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $id_cli = new Cliente($registro[1]);
            $id_cli -> consultar();
            $this -> id_cli = $id_cli;
            $id_tend = new Tienda($registro[2]);
            $id_tend -> consultar();
            $this -> id_tend = $id_tend;
            $id_prod = new Producto($registro[3]);
            $id_prod->consultar();
            $this->id_prod = $id_prod;
            $venta = new Venta($registro[0], $id_cli ,$id_tend, $id_prod,$registro[4],$registro[5],$registro[6]);
            array_push($ventas, $venta);
        }
        $this -> conexion -> cerrar();
        return  $ventas;



    }
    
    
    
}
?>