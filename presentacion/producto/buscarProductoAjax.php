<?php 
$filtro = $_GET["filtro"];
if($filtro=='tod'){
    $producto = new Producto();
    $productos = $producto -> consultarTodos();
}else{
    $producto = new Producto();
    $productos = $producto -> buscar($filtro);
}

?>
<table class="table table-striped table-hover">
<thead>
             <tr>
                 <th>ID</th>
                 <th>NOMBRE</th>
                 <th>TIENDA</th>
                 <th>IMAGEN</th>
                 <th>VALOR</th>
                 <th>DESCRIPCION</th>
                
             </tr>
         </thead>
         <tbody>
             <?php foreach ($productos as $productoActu) {
             ?>
             <tr>
             	 <td><?php echo $productoActu->getId()?></td>
                 <td><?php echo $productoActu->getNombre()?></td>
                 <td><?php echo $productoActu->getId_tien()->getNombre()." (".$productoActu->getId_tien()->getCod().")"?></td>
                 <td>
                    <img src="presentacion/img/<?php echo $productoActu->getImagen();?>" width="150" height="150" alt=""/>
                    

                     </td>
                 <td><?php echo $productoActu->getValor();?></td>
                 
                 <td><?php echo $productoActu->getDescripcion();?></td>
                 
             </tr>
            <?php }?>
     
           
            
         </tbody>
     </table>
