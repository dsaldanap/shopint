<?php 
$dueño=new Dueño($_SESSION["id"]);//recibe el id administrador
$dueño->consultar();//consulta los dueños disponibles
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">

    <a class="navbar-brand">ShopInt</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link active" aria-current="page"
                    href="index.php?pid=<?php echo base64_encode("presentacion/producto/verTiendasD.php") ?>">Tienda</a>
            </li>
            
            <li class="nav-item dropdown">
                <a class="nav-link active dropdown-toggle" href="#" id="navbarScrollingDropdown" role="button"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    Informes
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarScrollingDropdown">
                    <li><a class="dropdown-item"
                            href="index.php?pid=<?php echo base64_encode("presentacion/informes/informe.php") ?>">Consultar
                            ventas tienda
                        </a></li>
                        <li><a class="dropdown-item"
                        href="index.php?pid=<?php echo base64_encode("presentacion/informes/buscarVentas.php") ?>">Consultar
                            ventas tienda Ajax
                        </a></li>
                    <li><a class="dropdown-item"href="index.php?pid=<?php echo base64_encode("presentacion/informes/comprasclientes.php") ?>">Consultar Compras Cliente
                        </a></li>
                        <li><a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/informes/buscarCompra.php") ?>">Consultar Compras ClienteAjax
                        </a></li>
                    <li><a class="dropdown-item" href="reporte.php" target="_blank">Reporte Ventas tienda
                        </a></li>
                    <li><a class="dropdown-item"  href="reportecompras.php" target="_blank">Reporte Compras Cliente
                        </a></li>
                    <li><a class="dropdown-item"href="index.php?pid=<?php echo base64_encode("presentacion/producto/estadisticas/estadisticas2.php") ?>" >Estadisticas
                        </a></li>
                </ul>
            </li>



        </ul>
    </div>
    <div class="nav-item dropdown col-1">
        <button type="button" class="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
            <?php echo $dueño->getNombre()?>
        </button>
        <div>
        </div>
        <div class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton"">
           <a class=" dropdown-item">
            <img src="presentacion/img/user1.png" alt="user" higth="60" width="60" />
            </a>

            <li><a class="dropdown-item"><?php echo $dueño->getUsuario()?></a></li>
            <li><a class="dropdown-item">Dueño</a></li>
            <li><a class="dropdown-item" href="index.php?sesion=false">Cerrar Sesion</a></li>
        </div>

    </div>



</nav>