<?php 
require_once 'logica/Administrador_ma.php';//Importar Administrador y sus funciones
require_once 'logica/Cliente.php';//Importar Cliente y sus funciones
require_once 'logica/Tendero.php';
require_once 'logica/Tienda.php';
require_once 'logica/Producto.php';//Importar Tienda y sus funciones
require_once 'logica/Carrito.php';
require_once 'logica/Venta.php';
require_once 'logica/Compra.php';
require_once 'fpdf/fpdf.php';
$compra= new Compra();

$compras= $compra->consultarTodos();

$pdf = new FPDF('P','mm', 'Letter');
$pdf -> SetMargins(10, 10, 10);
$pdf -> AddPage();

$pdf -> SetFont('Times', 'B', 18);
$pdf -> Cell(196, 10, "SHOPINT", 0, 1, 'C');
$pdf -> Cell(196, 20, "Reporte compras clientes", 0, 1, 'C');

$pdf -> SetFont('Times', 'B', 10);

$pdf -> Cell(33, 8, " ", 0, 0, 'C');
$pdf -> Cell(10, 8, "#", 1, 0, 'C');
$pdf -> Cell(30, 8, "Nombre Cliente", 1, 0, 'C');
$pdf -> Cell(30, 8, "Fecha Compra", 1, 0, 'C');
$pdf -> Cell(30, 8, "#De productos", 1, 0, 'C');
$pdf -> Cell(30, 8, "Total pagado", 1, 1, 'C');


$pdf -> SetFont('Times', '', 10);
$i = 1;
foreach ($compras as $compraActual){
  $pdf -> Cell(33, 8, " ", 0, 0, 'C');
    $pdf -> Cell(10, 8, $i++, 1, 0, 'C');
    $pdf -> Cell(30, 8,  $compraActual ->getId_clie()->getNombre() , 1, 0, 'C');
    $pdf -> Cell(30, 8, $compraActual -> getFecha() , 1, 0, 'C');
    $pdf -> Cell(30, 8,  $compraActual -> getCantidad() , 1, 0, 'C');
    $pdf -> Cell(30, 8, "$".$compraActual -> getMontoto() , 1, 1, 'C');
  }
$pdf -> Output('I');
?>