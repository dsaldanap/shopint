<?php
include 'presentacion/vistaCli.php';

$id_clie=$cliente->getId();
$compra= new Compra("",$id_clie,"","", "");
$compras= $compra->consultarCompras();
$i = 1;
if(isset($_POST["eli"])){
    $name=$_POST["eli"];
    $compra2= new Compra($name,"","","", "");
    $compras2= $compra2->borrar();
   
   
  
}
?>
<div class="container">
    <div class="row mt-3">

        <div class="col-12">
            <br>
            <div class="card">

                <h3 class="card-header text-center">Mis Compras</h3>
                <form method="post"
                    action="index.php?pid=<?php echo base64_encode("presentacion/producto/P_cliente/misCompras.php")?>">
                    <div class="card-body">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th># Compra </th>
                                    <th>Fecha compra</th>
                                    <th>Numero de Productos</th>
                                    <th>Total</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">

                                <?php 
							
							foreach ($compras as $compraActual){?>
                                <tr>
                                   
                                    <td><?php echo $i++?></td>
                                    <td><?php echo  $compraActual -> getFecha() ?></td>
                                    <td><?php echo  $compraActual ->getCantidad()?></td>
                                    <td><?php echo "$".$compraActual -> getMontoto()?></td>


                                    <td>

                                        <button value="<?php echo $compraActual->getId();?>" name="eli"
                                            class="btn btn-outline-danger text-center">Eliminar</button>
                                    </td>


                                </tr>
                                <?php	}?>
                            </tbody>
                        </table>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
