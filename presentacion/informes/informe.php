<?php include 'presentacion/vistaDue.php';

$venta= new Venta();

$ventas= $venta->consultarTodos();
?>
<div class="container">
    <div class="row mt-3">
        
        <div class="col-12">
        <br>
            <div class="card">
               
                <h3 class="card-header text-center">Consultar Ventas</h3>
                <div class="card-body">
    <table class="table table-hover text-center">
         <thead>
             <tr>
                 <th># Venta </th>
                 <th>Nombre tienda</th>
                 <th>Fecha venta</th>
                 <th>Nombre Cliente</th>
                 <th>Producto</th>
                 
                 <th>Cantidad</th>
                 <th>Total</th>
             </tr>
         </thead>
         <tbody class="text-center">
                                <?php 
							$i = 1;
							foreach ($ventas as $ventaActual){
							    echo "<tr>";
							    echo "<td>" . $i++ . "</td>";
							    echo "<td>" . $ventaActual ->getId_tend()->getNombre() . "</td>";
                                echo "<td>" . $ventaActual -> getFecha() . "</td>";
							    echo "<td>" . $ventaActual ->getId_cli()-> getNombre() . "</td>";
                                echo "<td>" . $ventaActual ->getId_pro()->getNombre(). "</td>";
							    echo "<td>" . $ventaActual -> getCantidad() . "</td>";
                                echo "<td>" . "$".$ventaActual -> getTotal() . "</td>";
							    echo "</tr>";							    
							}
						?>
                            </tbody>
     </table>

                </div>
            </div>
    </div>
</div>