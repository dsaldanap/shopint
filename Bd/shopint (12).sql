-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-03-2022 a las 03:17:17
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `shopint`
--
CREATE DATABASE IF NOT EXISTS `shopint` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `shopint`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador_ma`
--

CREATE TABLE `administrador_ma` (
  `id_admin` int(11) NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `usuario` varchar(40) DEFAULT NULL,
  `clave` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador_ma`
--

INSERT INTO `administrador_ma` (`id_admin`, `nombre`, `usuario`, `clave`) VALUES
(2, 'David Aldana', 'DSAP', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id_carrito` int(11) NOT NULL,
  `id_prod` int(11) NOT NULL,
  `id_cli` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `monto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `clave` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `usuario`, `clave`) VALUES
(2, 'Roberto Aguilar', 'Ra', '250cf8b51c773f3f8dc8b4be867a9a02'),
(3, 'Pablo Gomez', 'pa', '202cb962ac59075b964b07152d234b70'),
(4, 'Juan Casas', 'jc', '202cb962ac59075b964b07152d234b70'),
(5, 'Estefania Castro', 'cast', '202cb962ac59075b964b07152d234b70'),
(6, 'Camilo Sexto', 'Cami', '202cb962ac59075b964b07152d234b70'),
(7, 'Tatiana Aguilar', 'Tati', '202cb962ac59075b964b07152d234b70'),
(8, 'Santiago Gomez', 'Sant', '202cb962ac59075b964b07152d234b70'),
(9, 'Sofia Hernandez', 'Sofi', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_cli`
--

CREATE TABLE `compra_cli` (
  `id_compra` int(11) NOT NULL,
  `cl_id_clie` int(11) NOT NULL,
  `fecha_compra` date NOT NULL,
  `numero_productos` int(11) NOT NULL,
  `monto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `compra_cli`
--

INSERT INTO `compra_cli` (`id_compra`, `cl_id_clie`, `fecha_compra`, `numero_productos`, `monto`) VALUES
(10, 2, '2022-03-27', 2, 15000),
(14, 5, '2022-03-27', 4, 62200),
(15, 5, '2022-03-27', 1, 10000),
(16, 5, '2022-03-27', 1, 9000),
(17, 6, '2022-03-27', 2, 100300),
(18, 6, '2022-03-27', 3, 18000),
(19, 7, '2022-03-27', 1, 2000),
(20, 7, '2022-03-27', 1, 10000),
(21, 8, '2022-03-27', 1, 18000),
(22, 8, '2022-03-27', 3, 124000),
(23, 9, '2022-03-27', 1, 30000),
(24, 2, '2022-03-28', 2, 15600),
(33, 2, '2022-03-30', 2, 51000),
(34, 6, '2022-03-30', 1, 600);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dueño`
--

CREATE TABLE `dueño` (
  `id_du` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `dueño`
--

INSERT INTO `dueño` (`id_du`, `nombre`, `usuario`, `clave`) VALUES
(1, 'Sebastian Avendaño', 'seb', '7812e8b74f6837fba66f86fe86688a2b');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_prod` int(11) NOT NULL,
  `t_id_Tienda` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `valor` int(20) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_prod`, `t_id_Tienda`, `nombre`, `imagen`, `valor`, `descripcion`) VALUES
(1, 1, 'Pan de queso', '1647713037_panqueso.jpg', 300, 'Pan relleno de queso'),
(2, 1, 'Costeño', '1647713093_costeño.jpg', 2000, 'Pan relleno de bocadillo'),
(3, 2, 'Fresas', '1647713121_fresa.jpg', 3000, 'Libra de fresas'),
(4, 2, 'Moras', '1647713145_moras.jpeg', 4500, 'Libra de moras'),
(5, 3, 'Pastel de frutas', '1647713177_paste.jpg', 15000, 'Pastel hecho de frutos rojos'),
(6, 3, 'Pastel de limon', '1647713219_lima.jpg', 10000, 'Pastel hecho con limon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tendero`
--

CREATE TABLE `tendero` (
  `id_tendero` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `clave` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tendero`
--

INSERT INTO `tendero` (`id_tendero`, `nombre`, `usuario`, `clave`) VALUES
(1, 'Enrique Rodriguez', 'Enri', '9cfefed8fb9497baa5cd519d7d2bb5d7'),
(2, 'Maria del Carmen', 'Mace', '202cb962ac59075b964b07152d234b70'),
(3, 'Juliana Escarraga', 'Juli', '202cb962ac59075b964b07152d234b70'),
(4, 'Roberto', 'Rober', '68053af2923e00204c3ca7c6a3150cf7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `id_tienda` int(11) NOT NULL,
  `cod` varchar(10) NOT NULL,
  `nombre` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`id_tienda`, `cod`, `nombre`) VALUES
(1, 'PAN', 'Panaderia Don Enrique'),
(2, 'SFE', 'Supermercado Felicidad Eterna'),
(3, 'REPO', 'Postres de Juliana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `cl_id_cliente` int(11) NOT NULL,
  `te_id_tendero` int(11) NOT NULL,
  `pr_id_pro` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `fecha_venta` date NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `cl_id_cliente`, `te_id_tendero`, `pr_id_pro`, `total`, `fecha_venta`, `cantidad`) VALUES
(35, 3, 2, 4, 22500, '2022-03-25', 5),
(36, 4, 2, 3, 12000, '2022-03-25', 4),
(37, 4, 2, 4, 45000, '2022-03-27', 10),
(39, 4, 3, 6, 70000, '2022-03-27', 7),
(40, 6, 1, 2, 20000, '2022-03-28', 10),
(41, 9, 3, 5, 150000, '2022-03-28', 10);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador_ma`
--
ALTER TABLE `administrador_ma`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id_carrito`),
  ADD KEY `id_prod` (`id_prod`),
  ADD KEY `id_cli` (`id_cli`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `compra_cli`
--
ALTER TABLE `compra_cli`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `id_clie` (`cl_id_clie`);

--
-- Indices de la tabla `dueño`
--
ALTER TABLE `dueño`
  ADD PRIMARY KEY (`id_du`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_prod`),
  ADD KEY `t_id_Tienda` (`t_id_Tienda`);

--
-- Indices de la tabla `tendero`
--
ALTER TABLE `tendero`
  ADD PRIMARY KEY (`id_tendero`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id_tienda`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `fk_id_tendero` (`te_id_tendero`),
  ADD KEY `fk_id_prod` (`pr_id_pro`),
  ADD KEY `cl_id_cliente` (`cl_id_cliente`,`te_id_tendero`,`pr_id_pro`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador_ma`
--
ALTER TABLE `administrador_ma`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id_carrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `compra_cli`
--
ALTER TABLE `compra_cli`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `dueño`
--
ALTER TABLE `dueño`
  MODIFY `id_du` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_prod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tendero`
--
ALTER TABLE `tendero`
  MODIFY `id_tendero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id_tienda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `fk_id_cli` FOREIGN KEY (`id_cli`) REFERENCES `cliente` (`id_cliente`);

--
-- Filtros para la tabla `compra_cli`
--
ALTER TABLE `compra_cli`
  ADD CONSTRAINT `fk_id_clie` FOREIGN KEY (`cl_id_clie`) REFERENCES `cliente` (`id_cliente`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_tienda_id` FOREIGN KEY (`t_id_Tienda`) REFERENCES `tienda` (`id_tienda`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_id_cliente` FOREIGN KEY (`cl_id_cliente`) REFERENCES `cliente` (`id_cliente`),
  ADD CONSTRAINT `fk_id_prod` FOREIGN KEY (`pr_id_pro`) REFERENCES `producto` (`id_prod`),
  ADD CONSTRAINT `fk_id_tendero` FOREIGN KEY (`te_id_tendero`) REFERENCES `tendero` (`id_tendero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
