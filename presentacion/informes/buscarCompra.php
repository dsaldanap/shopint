<?php
include 'presentacion/vistaDue.php';
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Buscar Compras</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-4"></div>
						<div class="col-4 text-center">
    						<div class="mb-3">							
    							<input type="text" class="form-control" id="filtro" placeholder="Digite palabra" >							
    						</div>
						</div>	
					</div>
					<div class="row">
						<div class="col">
							<div id="resultados"></div>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$( "#filtro" ).keyup(function() {
  filtro = $( "#filtro" ).val(); 
  if(filtro.length >= 3){
      url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/informes/buscarCompraAjax.php")?>&filtro=" + filtro;
      $( "#resultados" ).load(url);    
  }
});
</script> 


