<?php
require_once 'persistencia/Conexion.php'; // importar conexion
require_once 'persistencia/CarritoDAO.php';

// importar AdmistradorDao
class Carrito
{

    private $id;

    private $id_pro;
    private $id_cli;

    private $cantidad;
    private  $nombre;
    private $monto;

    private $conexion;

    private $carritoDAO;

    public function getId()
    {
        return $this->id;
    }
    public function getId_pro()
    {
        return $this->id_pro;
    }
    public function getId_cli()
    {
        return $this->id_cli;
    }
    public function getNombre()
    {
        return $this->nombre;
    }
  
    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function getMonto()
    {
        return $this->monto;
    }
    public function setId_pro($id_pro)
    {
        $this->id_pro= $id_pro;
    }
    public function setId($id)
    {
        $this->id= $id;
    }
    public function setId_cli($id_cli)
    {
        $this->id_cli= $id_cli;
    }
    public function setCantidad($cantidad)
    {
        $this->cantidad= $cantidad;
    }
    public function setMonto($monto)
    {
        $this->monto = $monto;
    }

    public function setDos($id_cli,$id_pro)
    {
        $this->id_cli= $id_cli;
        $this->id_pro= $id_pro;
    }
    
    public function __construct($id = "", $id_pro = "",   $id_cli = "",$nombre="",$cantidad = "", $monto = "")
    {
        $this->id = $id;
        $this->id_pro = $id_pro;
        $this->id_cli= $id_cli;
        $this->nombre=$nombre;
        $this->cantidad = $cantidad;
        $this->monto = $monto;
        $this->conexion = new Conexion();
        $this->carritoDAO = new CarritoDAO($this->id, $this->id_pro, $this->id_cli ,$this->nombre,$this->cantidad, $this->monto);
    }

    public function consultar()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carritoDAO->consultar());
        $this->conexion->cerrar();
        $datos = $this->conexion->extraer();
        $id_pro = new Producto($datos[0]);
        $id_pro->consultar();
        $this->id_pro = $id_pro;
        $id_cli = new Cliente($datos[1]);
        $id_cli->consultar();
        $this->id_cli = $id_cli;
        $this->nombre=$datos[2];
        $this->cantidad = $datos[3];
        $this->monto = $datos[4];
        $this->conexion->cerrar();
    }
    public function consultarCanti()
    {
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->carritoDAO->consultarCanti());
       
        $datos = $this->conexion->extraer();
        $this->id =($datos[0]);
        $id_pro = new Producto($datos[1]);
        $id_pro->consultar();
        $this->id_pro = $id_pro;
        $id_cli = new Cliente($datos[2]);
        $id_cli->consultar();
        $this->id_cli = $id_cli;
        $this->nombre=$datos[3];
        $this->cantidad = $datos[4];
        $this->monto = $datos[5];
        $this->conexion->cerrar();
    }
    public function consultarCanti2()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carritoDAO->consultarCanti());
        
        $datos = $this->conexion->extraer();
        if( $datos!=null){
           
            return true;
        }
        else{
            
            return false;
        }
       
        $this->conexion->cerrar();
    }

    public function consultarTodos()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->carritoDAO->consultarTodos());

        $carritos = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $id_pro = new Producto($registro[1]);
            $id_pro->consultar();
            $this->id_pro = $id_pro;
            $id_cli = new Cliente($registro[2]);
             $id_cli->consultar();
            $this->id_cli = $id_cli;
            
            $carrito = new Carrito($registro[0], $id_pro, $id_cli, $registro[3],$registro[4],$registro[5]);
            array_push($carritos, $carrito);
        }
        $this->conexion->cerrar();
        return $carritos;
    }

    public function consultarCar(){
        $this->conexion->abrir();
        
        
        $this->conexion->ejecutar($this->carritoDAO->consultarCar());
        
        $carritos = array();
        while (($registro = $this->conexion->extraer()) != null) {
            $id_pro = new Producto($registro[1]);
            $id_pro->consultar();
            $id_cli = new Cliente($registro[2]);
        $id_cli->consultar();
        $this->id_cli = $id_cli;
            $this->id_pro = $id_pro;
            $carrito = new Carrito($registro[0], $id_pro, $id_cli, $registro[3],$registro[4],$registro[5]);
            array_push($carritos, $carrito);
        }
        $this->conexion->cerrar();
        return $carritos;
        
        
    }
    public function borrar()
    {
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->carritoDAO->borrar()); // crear producto

        $this->conexion->cerrar();
    }
    public function actualizar()
    {
        $this->conexion->abrir();
        
        $this->conexion->ejecutar($this->carritoDAO->actualizar()); // crear producto
        $id_pro= new Producto($this -> id_pro);
        $id_pro -> consultar();
        $this -> id_pro = $id_pro;
        $id_cli = new Cliente($this -> id_cli );
        $id_cli->consultar();
        $this -> id_cli  = $id_cli ;
        $this->conexion->cerrar();
    }
    public function crear()
    {
        $this->conexion->abrir();

        $this->conexion->ejecutar($this->carritoDAO->crear()); // crear producto

        $this->conexion->cerrar();
    }
    public function borrar2()
    {
        $this->conexion->abrir();

        $this->carritoDAO-> setId_cli($this->id_cli);
        $this->conexion->ejecutar($this->carritoDAO->borrar2()); // crear producto

        $this->conexion->cerrar();
    }
}
?>