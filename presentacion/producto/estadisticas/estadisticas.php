<?php
include 'presentacion/vistaDue.php';

$venta= new Venta();

$ventas= $venta->consultarTodos();

$datos = array();
foreach ($ventas as $ventaActual){
    if(!array_key_exists($ventaActual ->getId_pro()->getNombre(), $datos)){
        $datos[$ventaActual -> getId_pro()->getNombre() ] = 1;
    }else{
        $datos[$ventaActual -> getId_pro()->getNombre() ]++;
    }
}


?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Estadisticas</h5>
				<div class="card-body">
					<div id="piechart" style="height: 500px;"></div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Producto', 'Cantidad'],
      <?php 
          foreach ($datos as $key => $value){
              echo "['" . $key . "', " . $value . "],";
          }      
      ?>
    ]);
    
    var options = {
      title: 'Productos vendidos por tienda'
    };    
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));    
    chart.draw(data, options);
}
</script>
