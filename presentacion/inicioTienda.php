<?php include 'presentacion/vistaTienda.php'; 
$id_t=$tendero->getId();
if($id_t=='1'){
  $nombret="Panaderia Don Enrique";
}else if($id_t=='2'){
  $nombret="Supermercado Felicidad Eterna";
}
else if($id_t=='3'){
  $nombret="Postres de Juliana";
}

?>

<div class="jumbotron">
  <h1 class="display-3">Bienvenido <?php echo $tendero->getNombre()?></h1>
  
  <hr class="my-2">
  <p>Tendero</p>
  <p><?php echo $nombret?></p>
  
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="index.php?pid=<?php echo base64_encode("presentacion/producto/verTienda.php") ?>" role="button">Ver Todos Los Productos</a>
  </p>
</div>